<?php

namespace Kisphp\Orm;

use Kisphp\Singleton;

/**
 * @method OrmErrors OrmErrors::getInstance()
 */
class OrmErrors
{
    use Singleton;

    /**
     * @var array $fieldsWithErrors
     */
    protected $fieldsWithErrors = [];

    /**
     * @var array $errors
     */
    protected $errors = [];

    /**
     * @param string $error
     *
     * @return OrmErrors
     */
    public function addError($error)
    {
        $this->errors[] = $error;

        return $this;
    }

    /**
     * @param string $field
     *
     * @return OrmErrors
     */
    public function addField($field)
    {
        if ( ! in_array($field, $this->fieldsWithErrors)) {
            $this->fieldsWithErrors[] = $field;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getFieldsWithErrors()
    {
        return $this->fieldsWithErrors;
    }

    public function toArray()
    {
        $errors = [];
        foreach ($this->getErrors() as $errorItem) {
            $errors[] = $errorItem;
        }

        $fieldsWithErrors = [];
        foreach ($this->getFieldsWithErrors() as $fieldItem) {
            $fieldsWithErrors[] = $fieldItem;
        }

        return [
            'errors' => $errors,
            'fields' => $fieldsWithErrors,
        ];
    }
}
