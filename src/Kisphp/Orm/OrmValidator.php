<?php

namespace Kisphp\Orm;

use Kisphp\Orm\OrmErrors;
use Kisphp\Orm\OrmInterface;
use Kisphp\Database\Kisdb;

class OrmValidator
{
    protected $valid = true;
    protected $object;

    /**
     * @param $object
     */
    public function __construct(OrmInterface $object)
    {
        $this->errors = OrmErrors::getInstance();
        $this->object = $object;
    }

    /**
     * generates get method for the field
     *
     * @param $name
     * @return string
     */
    protected function _get_method($name)
    {
        $name = preg_replace('/([_]+)/', ' ', $name);
        $name = ucwords(strtolower($name));

        return 'get' . strtr($name, array(' ' => ''));
    }

    /**
     * create rule for the field
     *
     * @param $field
     * @param $rules
     */
    public function rule($field, $rules)
    {
        $method = $rules[0];
        $this->$method($field, $rules);
    }

    /**
     * @param string $field
     * @param array $rules
     *
     * @todo check return if $this->object->getId() > 0
     */
    public function unique($field, array $rules)
    {
        $db = Kisdb::getInstance();

        $method = $this->_get_method($field);
        $class = get_class($this->object);


        if ( $this->object->getId() > 0 ) {
            return;
        }

        $check = Orm::getRepository($class)->getBy([
            $db->secure($field) => $this->object->$method(),
        ]);

        if ( intval($check) > 0 ) {
            if ( ! isset($rules[2]) || ! in_array($rules[2], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[1], $field));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * @param string $field
     * @param array $rules
     */
    public function required($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( trim($this->object->$method()) == '' ) {
            if ( ! isset($rules[2]) || ! in_array($rules[2], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[1], $field));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * @param string $field
     * @param array $rules
     */
    public function minLength($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( strlen($this->object->$method()) < $rules[1] ) {
            if ( ! isset($rules[3]) || ! in_array($rules[3], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[2], $field, $rules[1]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * @param string $field
     * @param array $rules
     */
    public function email($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( ! filter_var($this->object->$method(), FILTER_VALIDATE_EMAIL) ) {
            if ( isset($rules[2]) && ! in_array($rules[2], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[1], $field));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * 0 => this function name
     * 1 => min size
     * 2 => error message to display
     * 3 => skip this error if specified field has error
     *
     * @param string $field
     * @param array $rules
     */
    public function minCount($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( count($this->object->$method()) < $rules[1] ) {
            if ( ! in_array($rules[3], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[2], $rules[1]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * 0 => this function name
     * 1 => min size
     * 2 => error message to display
     * 3 => skip this error if specified field has error
     *
     * @param string $field
     * @param array $rules
     */
    public function maxCount($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( count($this->object->$method()) > $rules[1] ) {
            if ( ! in_array($rules[3], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[2], $rules[1]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * 0 => this function name
     * 1 => min length
     * 2 => error message to display
     * 3 => skip this error if specified field has error
     *
     * @param $field
     * @param array $rules
     */
    public function min($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( $this->object->$method() < $rules[1] ) {
            if ( ! isset($rules[3]) || ! in_array($rules[3], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[2], $field, $rules[1]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * 0 => this function name
     * 1 => min length
     * 2 => error message to display
     * 3 => skip this error if specified field has error
     *
     * @param string $field
     * @param array $rules
     */
    public function max($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( $this->object->$method() > $rules[1] ) {
            if ( ! isset($rules[3]) || ! in_array($rules[3], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[2], $field, $rules[1]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * 0 => this function name
     * 1 => error message to display
     *
     * @param string $field
     * @param array $rules
     */
    public function captcha($field, array $rules)
    {
        $request = Request::createFromGlobals();

        if ( md5($request->request->get($field)) != md5($request->session->get($field)) || trim($request->request->get($field)) == '' ) {
            $this->errors->addError($rules[1]);
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * @param string $field
     * @param array $rules
     */
    public function between($field, array $rules)
    {
        $method = $this->_get_method($field);
        if ( $this->object->$method() < $rules[1] || $this->object->$method() > $rules[2] ) {
            if ( ! isset($rules[4]) || ! in_array($rules[4], $this->errors->getFieldsWithErrors()) ) {
                $this->errors->addError(sprintf($rules[3], $field, $rules[1], $rules[2]));
            }
            $this->errors->addField($field);
            $this->valid = false;
        }
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $_rules = $this->object->getRules();

        foreach ( $_rules as $column => $rules ) {
            if ( is_array($rules[0]) ) {
                foreach ($rules as $rule) {
                    $this->rule($column, $rule);
                }
            } else {
                $this->rule($column, $rules);
            }
        }
        return $this->valid;
    }
}
