<?php

namespace Kisphp\Orm;

interface OrmInterface
{
    /**
     * @param array $dataArray
     *
     * @return OrmInterface
     */
    public function fromArray(array $dataArray);

    /**
     * @return array
     */
    public function toArray();
}
