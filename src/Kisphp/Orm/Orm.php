<?php

namespace Kisphp\Orm;

use Kisphp\Database\Kisdb;
use Kisphp\Orm\OrmInterface;
use Symfony\Component\Finder\Finder;

abstract class Orm implements OrmInterface
{
    /**
     * @var array
     */
    protected $_rules = [];

    /**
     * @var null
     */
    private $_validate_catpcha = null;

    /**
     * @return string
     */
    protected function getTableName()
    {
        if ( defined('static::TABLE') ) {
            return static::TABLE;
        } else {
            $name = get_called_class();
            $name = strtolower($name);

            return $name;
        }
    }

    /**
     *
     */
    public function close()
    {
        unset($this);
    }

    /**
     * @param string $name
     *
     * @return OrmInterface
     */
    public static function getRepository($name)
    {
        $repository = self::findRepositoryByName($name);

        return new $repository();
    }

    /**
     * @param string $name
     * @return string
     * @throws \Exception
     */
    protected static function findRepositoryByName($name)
    {
        $extension = '.php';
        $finder = new Finder();
        $filesInCore = $finder->files()
            ->name($name . $extension)
            ->in([
                CORE . '/Class/Orm/',
            ])
        ;

        if ($filesInCore->count() > 0) {
            foreach ($filesInCore as $fc) {
                return strtr($fc->getRelativePathname(), [
                    $extension => '',
                ]);
            }
        }

        // search in src/Project/Bundles
        $filesInVendors = glob(SYSTEM . '/src/*/*/Persistance/Entities/');
        foreach ($filesInVendors as $fiv) {
            $fullPath = $fiv . $name . $extension;
            if (is_file($fullPath)) {
                return self::getProjectBundleNamespace($fullPath);
            }
        }

        // search in vendor/kisphp bundles
        $filesInVendors = glob(SYSTEM . '/vendor/kisphp/*/src/*/Persistance/Entities/');
        foreach ($filesInVendors as $fiv) {
            $fullPath = $fiv . $name . $extension;
            if (is_file($fullPath)) {
                return self::getKisphpBundleNamespace($fullPath);
            }
        }

        throw new \Exception(
            sprintf('%s entity could not be found in the system.', $name)
        );
    }

    /**
     * @param string $filePath
     * @return string
     */
    public static function getProjectBundleNamespace($filePath)
    {
        $filePath = strtr($filePath, [
            SYSTEM => '',
            '.php' => '',
        ]);

        $filePath = self::removePartOfString($filePath, 'src/');

        return strtr($filePath, [
            '/' => '\\',
        ]);
    }

    /**
     * @param string $filePath
     * @return string
     */
    public static function getKisphpBundleNamespace($filePath)
    {
        $filePath = strtr($filePath, [
            SYSTEM . '/vendor/kisphp/' => '',
            '.php' => '',
        ]);

        $filePath = self::removePartOfString($filePath, 'src/');

        return strtr($filePath, [
            '/' => '\\',
        ]);
    }

    /**
     * @param string $string
     * @return string
     */
    private static function removePartOfString($string, $search)
    {
        $limit = strpos($string, $search);

        $string = substr($string, $limit + strlen($search));
        return $string;
    }

    /**
     * @param string $field
     * @param array $rules
     */
    public function addRule($field, Array $rules=[])
    {
        $this->_rules[$field] = $rules;
    }

    /**
     * @param array $data
     *
     * @return OrmInterface
     */
    public function fromArray(array $data)
    {
        $vars = $this->__get_columns();

        foreach ($vars as $v) {
            if (property_exists($this, $v) && array_key_exists($v, $data)) {
                $this->$v = $data[$v];
            }
        }

        return $this;
    }

    /**
     * get the database result as an array
     *
     * @return array
     */
    public function toArray()
    {
        $vars = $this->__get_columns();
        $output = [];
        if ( count($vars) > 0 ) {
            foreach ($vars as $col) {
                if ( strpos($col, '_') !== 0) {
                    $output[$col] = $this->$col;
                }
            }
        }
        return $output;
    }

    /**
     * get result from database by primary key
     *
     * @param int $id
     * @param string $field
     *
     * @return Orm
     * @throws \Exception
     */
    public function get($id, $field='id')
    {
        if ( intval($id) < 0 ) {
            throw new \Exception('Primary key must be positive number');
        }
        $db = Kisdb::getInstance();

        $sql = sprintf(
            "SELECT * FROM `%s` WHERE `%s` = '%d' LIMIT 1",
            $this->getTableName(),
            $db->secure($field),
            $db->secure($id)
        );

        $query = $db->query($sql);

        return $query->fetch_object(get_called_class());
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getOneBy(array $parameters)
    {
        $result = $this->getBy($parameters);

        if (empty($result)) {
            return new FalseEntity();
        }

        return $result[0];
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function getBy(array $parameters)
    {
        $output = [];
        $db = Kisdb::getInstance();

        $sql = sprintf("SELECT * FROM `%s` WHERE ", $this->getTableName());
        $pieces = [];
        foreach ($parameters as $k => $v) {
            $pieces[] = sprintf(" `%s` = '%s' ", $db->secure($k), $db->secure($v));
        }

        $query = $db->query( $sql . implode('AND', $pieces) );

        while ($row = $query->fetch_object(get_called_class())) {
            $output[] = $row;
        }

        return $output;
    }

    /**
     * Gets all records from a table and return object
     *
     * @param int $limit
     * @param int $startFrom
     * @return array
     */
    public function all($limit=0, $startFrom=0)
    {
        $result = [];
        $_limit = abs( intval($limit) );
        $_start_from = abs( intval($startFrom) );

        $db = Kisdb::getInstance();

        $sql = "SELECT * FROM `" . $this->getTableName() . "` ";

        if ( intval($limit) > 0 ) {
            if ( $_start_from > 0 ) {
                $sql .= sprintf(" LIMIT %d", $_limit);
            } else {
                $sql .= sprintf(" LIMIT %d, %d", $_start_from, $_limit);
            }
        }

        $query = $db->query($sql);

        while ($row = $query->fetch_object(get_called_class())) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Delete a record from database or marks it as deleted if status column exists
     */
    public function remove()
    {
        $db = Kisdb::getInstance();

        // get columns
        $vars = $this->__get_columns();

        if ( in_array('status', $vars) ) {
            $db->update($this->getTableName(), ['status' => 0], $this->id);
        } else {
            $db->query(sprintf(
                "DELETE FROM `%s` WHERE `id` = %d LIMIT 1",
                $this->getTableName(),
                intval($this->id)
            ));
        }
    }

    /**
     * save current object into database
     *
     * @param bool $ignore
     * @param bool $forceInsert
     *
     * @return bool
     */
    public function save($ignore=false, $forceInsert=false)
    {
        $db = Kisdb::getInstance();

        $_id = intval($this->id);

        // set created_at time
        if ( isset($this->created_at) && intval($this->id) == 0 ) {
            $now = new \DateTime('now', new \DateTimeZone(TIME_ZONE));
            $this->setCreatedAt($now->format('Y-m-d H:i:s'));
        }

        // set modified at time
        if ( isset($this->modified_at) && intval($this->id) > 0 ) {
            $now = new \DateTime('now', new \DateTimeZone(TIME_ZONE));
            $this->setModifiedAt($now->format('Y-m-d H:i:s'));
        }

        // set registered time
        if ( isset($this->registered) && intval($this->id) == 0 ) {
            $now = new \DateTime('now', new \DateTimeZone(TIME_ZONE));
            $this->setRegistered($now->format('Y-m-d H:i:s'));
        }

        if ( $this->_validate_catpcha !== null ) {
            $this->_rules[$this->_validate_catpcha] = [
                'captcha', Translate::get('captcha code is wrong')
            ];
        }

        $data = $this->__build_db_array($forceInsert);

        if ( $this->isValid() ) {
            if ( $_id > 0 && $forceInsert === false ) {
                $db->update($this->getTableName(), $data, $_id);
            } else {
                $this->id = $db->insert($this->getTableName(), $data, $ignore);
            }
            return true;
        }
        return false;
    }

    public function validateCaptcha($fieldName='captcha')
    {
        $this->_validate_catpcha = $fieldName;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $nv = new OrmValidator($this);
        if ($nv->isValid()) {
            return true;
        }

        return false;
    }

    /**
     * get rules declared for object
     *
     * @return array
     */
    public function getRules()
    {
        return $this->_rules;
    }

    /**
     * get mapping columns for database operations
     *
     * @param bool $getNotColumns
     *
     * @return array
     */
    private function __get_columns($getNotColumns=false)
    {
        $class = get_called_class();
        $vars = array_keys(get_class_vars($class));

        if ( count($vars) ) {
            foreach ($vars as $k => $v) {
                if ( true === $getNotColumns ) {
                    if ( strpos($v, '_') !== 0 ) {
                        unset($vars[$k]);
                    }
                } else {
                    if ( strpos($v, '_') === 0 ) {
                        unset($vars[$k]);
                    }
                }
            }
        }
        return $vars;
    }

    /**
     * @param bool $forceInsert
     *
     * @return array
     */
    private function __build_db_array($forceInsert=false)
    {
        $tmp = [];
        $vars = $this->__get_columns();

        if ( count($vars) > 0 ) {
            foreach ($vars as $col) {
                if ( ($col == 'id' && $forceInsert === false) || strpos($col, '_') === 0 ) {
                    continue;
                }
                $tmp[$col] = $this->$col;
            }
        }

        return $tmp;
    }
}
