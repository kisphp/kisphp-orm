<?php

namespace Kisphp\Orm;

/**
 * Class FalseEntity
 *
 * this class is used as an empty response from ORM
 */
class FalseEntity extends Orm
{
    function __call($name, $arguments)
    {
        return false;
    }
}
