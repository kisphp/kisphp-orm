<?php

use Kisphp\Orm\Orm;

class ValidatorEntity extends Orm
{
    protected $id;
    protected $status = 2;
    protected $title;
    protected $user_email;
    protected $registered;
    protected $password;

    protected $_rules = array(
        'title' => array('required', 'this is required message'),
        'user_email' => array(
            array('required', 'nu ati adaugat adresa de email'),
            array('email', 'adresa de email nu este valida', 'email'),
        ),
        'password' => array(
            array('required', 'the password is required'),
            array('min', 6, 'minim %d characters'),
            array('max', 12, 'maxim %d characters'),
        ),
        'status' => array('between', 0, 2, 'status number should be between %d and %d'),
    );

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * @param mixed $user_email
     */
    public function setUserEmail($user_email)
    {
        $this->user_email = $user_email;
    }

    /**
     * @return mixed
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param mixed $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }



}
