<?php

include_once __DIR__.'/Entity/DummyEntity.php';

use Kisphp\Database\Kisdb;
use Kisphp\Orm\Orm;

class OrmEntityTest extends PHPUnit_Framework_TestCase
{
    protected $db;

    public function setUp()
    {
        $this->db = Kisdb::getInstance();
        $this->db->connect('localhost', 'root', 'debian', 'test');
        $this->db->enableDebug();
        $this->db->enableMysqlDebug();
        $this->db->setDebugLog(__DIR__.'/mysql.log');

        $this->db->query("DROP TABLE IF EXISTS `dummy`");

        $createSql = "CREATE TABLE `dummy` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `status` TINYINT(4) NULL DEFAULT '2',
            `first_name` VARCHAR(50) NULL DEFAULT '',
            `last_name` VARCHAR(50) NULL DEFAULT '',
            `email` VARCHAR(50) NULL DEFAULT '',
            `password` CHAR(32) NULL DEFAULT '',
            PRIMARY KEY (`id`),
            INDEX `status` (`status`)
        )
        ENGINE=InnoDB;";
        $this->db->query($createSql);

    }

    public function tearDown()
    {
        $this->db->query("DROP TABLE dummy");
        $filename = $this->db->getDebugLog();
        if ( is_file($filename) ) {
            unlink($filename);
        }
        unset($this->db);
    }

    public function test_insert()
    {
        $d = new DummyEntity();
        $d->setFirstName('first_name');
        $d->setLastName('last_name');
        $d->setPassword( md5('password') );
        $d->setEmail('user@email.com');

        $this->assertTrue( (bool) $d->save());
    }

    public function test_insert_forced()
    {
        $d = new DummyEntity();
        $d->setId(100);
        $d->setFirstName('first_name');
        $d->setLastName('last_name');
        $d->setPassword( md5('password') );
        $d->setEmail('user@email.com');

        $this->assertTrue( (bool) $d->save(false, true));
    }

    public function _test_get()
    {
        $this->test_insert();
        $dum = (new DummyEntity())->get(1);

        $this->assertSame('DummyEntity', get_class($dum), 'row must have id = 1');
    }

    public function test_all()
    {
        $entityName = 'DummyEntity';

        for ($i=1; $i<=5; $i++) {
            $d = new $entityName();
            $d->setFirstName('first_name_'.$i);
            $d->setLastName('last_name_'.$i);
            $d->setPassword( md5('password_'.$i) );
            $d->setEmail('user_'.$i.'@email.com');

            $d->save();
        }

        $all = Orm::getRepository($entityName)->all();
        foreach ($all as $item) {
            $this->assertTrue( $item instanceof $entityName , 'Not an entity');
        }
    }

    /**
     * @depends test_insert
     */
    public function test_update()
    {
        $this->test_insert();

        $dum = (new DummyEntity())->get(1);

        $dum->setEmail('user_changed@email.com');

        $this->assertTrue( (bool) $dum->save());
    }
}
