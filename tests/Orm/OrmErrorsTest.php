<?php

use Kisphp\Orm\OrmErrors;

class OrmErrorsTest extends PHPUnit_Framework_TestCase
{
    protected $error;

    public function setUp()
    {
        $this->error = OrmErrors::getInstance();
        $this->error->addError('this is my error');
        $this->error->addField('my_field');
    }

    public function test_errors_stored()
    {
        $this->assertGreaterThan(0, count($this->error->getErrors()), 'should have error message');
        $this->assertGreaterThan(0, count($this->error->getFieldsWithErrors()), 'should have field for error');
    }

    public function test_to_array()
    {
        $err = OrmErrors::getInstance(true);
        $errors = [
            ['alfa','alfa_m'],
            ['beta','beta_m'],
        ];

        foreach ($errors as $v) {
            $err->addField($v[0]);
            $err->addError($v[1]);
        }

        $this->assertSame(
            '{"errors":["alfa_m","beta_m"],"fields":["alfa","beta"]}',
            json_encode($err->toArray())
        );
    }
}
